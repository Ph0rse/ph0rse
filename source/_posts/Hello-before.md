---
title: Hello_before
date: 2018-04-12 00:10:50
tags:
---
之前因为搞错了Github Pages的原理，导致之前的博文只剩下编译后的备份，想着手动去恢复为Markdown文件，但最近又很忙。
日子过得跟打仗似的，几个培训和分享挤到了一起，都得准备。Spring又连续爆洞，分析得眼晕……
在这里就先把最近的两篇文章同步一下，然后贴上之前投稿的文章地址~也差不多是之前写得比较好的文章了~
忽然觉得投稿是一个好习惯~

- 先知社区
[Java测试备忘录](https://xz.aliyun.com/t/2042)
[Java反序列化漏洞从入门到深入](https://xz.aliyun.com/t/2041)

- 信安之路
[一道反序列化CTF引起的思考](http://mp.weixin.qq.com/s/Z1zvcBg74T0psDGnXW-ebA)
[RedTigher WriteUp](http://mp.weixin.qq.com/s/nqfI10K423fO_KculsE8UQ)
[Mysql注入导图-学习篇](http://mp.weixin.qq.com/s/C0KKnl3suq0bzgFWFPzAXw)
[SQL注入的常规思路及奇葩技巧](http://mp.weixin.qq.com/s/hBkJ1M6LRgssNyQyati1ng)
[CTF初识与深入](http://mp.weixin.qq.com/s/1PrWW6mrRSCFOwl2xqDhmQ)
[Windows下优雅地书写MarkDown](http://mp.weixin.qq.com/s/ebNsriUThSvIWoZzV0xsNw)
[漏洞分析之Typecho二连爆](http://mp.weixin.qq.com/s/C9ojGt4TYZKX30lhTOT3VQ)

